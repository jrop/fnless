all:
	cargo build

release:
	cargo build --release
	strip target/release/fnless
	strip target/release/fnrun
	ls -lh target/release/fnless target/release/fnrun

run:
	cargo run --bin fnless -- serve --config-file ./fnless.toml

install: release
	sudo cp target/release/fnless /usr/local/bin/fnless
	sudo cp target/release/fnrun /usr/local/bin/fnrun
