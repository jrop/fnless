use std::net::SocketAddr;

use anyhow::{anyhow, Result};
use axum::{body::Body, extract::Request, http::StatusCode, response::Response};
use clap::Parser;
use fnless::*;
use log::{info, trace};
use tokio::sync::Mutex;

lazy_static::lazy_static! {
    static ref CLI_OPT: Mutex<Option<cli_opt::CliOpt>> = Mutex::new(None);
    static ref CONFIG: Mutex<Option<config::Config>> = Mutex::new(None);
}

pub async fn borrow_configuration<F, T>(f: F) -> T
where
    F: FnOnce(&cli_opt::CliOpt, &config::Config) -> T,
{
    let cli_options = CLI_OPT.lock().await;
    let cli_options = cli_options.as_ref().unwrap();
    let config = CONFIG.lock().await;
    let config = config.as_ref().unwrap();
    f(cli_options, config)
}

#[tokio::main]
async fn main() -> Result<()> {
    {
        let cli_options = cli_opt::CliOpt::parse();
        CLI_OPT.lock().await.replace(cli_options);
    }

    let cli_options_guard = CLI_OPT.lock().await;
    let cli_options = cli_options_guard.as_ref().unwrap();

    match &cli_options.subcommand {
        cli_opt::CliOptSubcommand::Install => {
            use std::process::Command;

            let systemd_service_file = include_str!("../../fnless.service");
            std::fs::write("/etc/systemd/system/fnless.service", systemd_service_file)?;

            let success = Command::new("systemctl")
                .args(["enable", "fnless.service", "--now"])
                .spawn()?
                .wait()?
                .success();

            if !success {
                return Err(anyhow!("systemctl enable ... did not exit successfully"));
            }
        }
        cli_opt::CliOptSubcommand::Serve(opts) => {
            {
                let config = config::read_config(opts)?;
                CONFIG.lock().await.replace(config);
                // drop the mutex guard
            }
            drop(cli_options_guard); // drop the mutex guard

            init_fern().await?;
            start_axum_server().await?;
        }
    }

    Ok(())
}

async fn init_fern() -> Result<()> {
    let log_file = borrow_configuration(|cli_opts, config| {
        cli_opts
            .subcommand
            .as_serve()
            .and_then(|serve| serve.log_file.as_ref())
            .or(config.log_file.as_ref())
            .cloned()
    })
    .await;

    let mut dispatch = fern::Dispatch::new()
        .format(|out, message, record| {
            out.finish(format_args!(
                "[{}] [{}] [{}] {}",
                chrono::Local::now().format("%Y-%m-%d %H:%M:%S"),
                record.level(),
                record.target(),
                message
            ))
        })
        .level(log::LevelFilter::Off)
        .level_for("fnless", log::LevelFilter::Trace)
        .chain(std::io::stdout());

    if let Some(log_file) = log_file {
        dispatch = dispatch.chain(fern::log_file(log_file)?)
    }

    dispatch.apply()?;
    Ok(())
}

async fn start_axum_server() -> Result<()> {
    let addr: SocketAddr = borrow_configuration(|cli_options, config| {
        let port = cli_options
            .subcommand
            .as_serve()
            .unwrap()
            .port
            .or(config.port)
            .unwrap_or(4545);
        ([127, 0, 0, 1], port).into()
    })
    .await;

    use axum::{routing::any, Router};
    let app = Router::new().fallback(any(http_request_handler));
    let listener = tokio::net::TcpListener::bind(addr).await.unwrap();
    info!("Listening on http://{}", addr);
    axum::serve(listener, app).await.unwrap();

    Ok(())
}

async fn http_request_handler(req: Request) -> Response {
    match http_request_handler_result(req).await {
        Ok(response) => response,
        Err(e) => {
            let mut resp = Response::new(Body::from(e.to_string()));
            *resp.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
            resp
        }
    }
}

async fn http_request_handler_result(req: Request) -> Result<Response> {
    let cgi_path = borrow_configuration(|cli_options, config| {
        let default_cgi_path = "/var/fnless".into();
        let cgi_path = cli_options
            .subcommand
            .as_serve()
            .unwrap()
            .cgi_path
            .as_ref()
            .or(config.cgi_path.as_ref())
            .unwrap_or(&default_cgi_path);
        cgi_path.to_owned()
    })
    .await;

    trace!("{:?} {:?} {:#?}", req.method(), req.uri(), req.headers());
    let script_name = req
        .uri()
        .path()
        .split('/')
        .find(|s| s != &"")
        .ok_or_else(|| anyhow!("script-name is required"))?;

    let cgi_script = cgi_script::CgiScript::new(script_name, cgi_path)?;
    let resp = cgi_script.execute(req).await?;
    Ok(resp)
}
