use anyhow::{anyhow, Context, Result};
use clap::Parser;
use std::{path::PathBuf, process::Command};

#[derive(Parser, Debug)]
#[clap(author = "Jonathan Apodaca", version = env!("CARGO_PKG_VERSION"))]
struct CliOpt {
    #[clap(required = true)]
    command: Vec<String>,
}

fn main() -> Result<()> {
    let opt = CliOpt::parse();
    let command = opt
        .command
        .first()
        .ok_or(anyhow!("a command is required"))?;
    let args: Vec<_> = opt.command.iter().skip(1).collect();

    let mut command_path = PathBuf::from(&command);
    if !command_path.exists() {
        let stdout = Command::new("which").arg(command).output()?.stdout;
        let stdout = String::from_utf8_lossy(&stdout);
        command_path = stdout.to_string().into();
    }

    let script = fnless::cgi_script::CgiScript::new("fnrun", command_path)?;
    let script_config = script.script_config()?;
    let empty_header_map = axum::http::HeaderMap::new();
    let env = script.env(&empty_header_map, &script_config)?;

    let status = Command::new(command)
        .args(args)
        .envs(env)
        .spawn()
        .context(format!("couldn't spawn: {command}"))?
        .wait()
        .context(format!("couldn't execute: {command}"))?;
    std::process::exit(status.code().unwrap_or(0));
}
