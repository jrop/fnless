use crate::cli_opt::CliOptServe;
use anyhow::Result;
use log::warn;
use std::path::PathBuf;

#[derive(Default, Debug, serde::Deserialize)]
pub struct Config {
    pub port: Option<u16>,
    pub cgi_path: Option<PathBuf>,
    pub log_file: Option<PathBuf>,
}

#[cfg(target_family = "unix")]
pub fn default_config_file_path() -> PathBuf {
    "/etc/fnless/config.toml".into()
}

pub fn read_config(cli_options: &CliOptServe) -> Result<Config> {
    let config_file_path = cli_options
        .config_file
        .clone()
        .unwrap_or_else(default_config_file_path);
    let config: Config = match std::fs::read_to_string(&config_file_path) {
        Ok(s) => toml::from_str(&s)?,
        Err(e) => {
            warn!(
                "could not read config file ({}): {}",
                config_file_path.display(),
                e
            );
            Config::default()
        }
    };
    Ok(config)
}
