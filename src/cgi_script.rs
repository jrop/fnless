use anyhow::{anyhow, Result};
use axum::{
    body::Body,
    extract::Request,
    http::{HeaderMap, HeaderName, HeaderValue},
    response::Response,
};
use futures::StreamExt;
use inflector::cases::screamingsnakecase::to_screaming_snake_case;
use log::{error, trace};
use std::process::Stdio;
use std::{collections::HashMap, path::PathBuf};
use tokio::io::AsyncBufReadExt;
use tokio::process::Command;
use tokio_util::io::ReaderStream;

#[derive(Debug, serde::Deserialize)]
pub struct CgiConfig {
    headers: Option<HashMap<String, String>>,
    env: Option<HashMap<String, String>>,
}

pub struct CgiScript {
    pub name: String,
    pub path: PathBuf,
    pub cgi_path: PathBuf,
}
impl CgiScript {
    pub fn new<S: AsRef<str>, P: Into<PathBuf>>(script_name: S, cgi_path: P) -> Result<Self> {
        let script_name = script_name.as_ref();
        if script_name.ends_with(".toml") {
            return Err(anyhow!("not found"));
        }

        let cgi_path = cgi_path.into();

        let mut script_path = cgi_path.clone();
        script_path.push(script_name);

        if !script_path.exists() {
            return Err(anyhow!(
                "the script {:?} does not exist",
                script_path.display()
            ));
        }

        Ok(CgiScript {
            name: script_name.into(),
            path: script_path,
            cgi_path,
        })
    }

    pub fn script_config(&self) -> Result<CgiConfig> {
        let mut script_toml_path = self.path.clone();
        script_toml_path.set_extension(match script_toml_path.extension() {
            Some(ext) => format!("{}.toml", ext.to_string_lossy()),
            None => "toml".to_string(),
        });
        if !script_toml_path.exists() {
            return Ok(CgiConfig {
                headers: None,
                env: None,
            });
        }

        let config: CgiConfig = toml::from_str(&std::fs::read_to_string(script_toml_path)?)?;
        Ok(config)
    }

    pub fn env(
        &self,
        headers: &HeaderMap,
        script_config: &CgiConfig,
    ) -> Result<HashMap<String, String>> {
        let mut env_vars = self.env_from_headers(headers);
        let default_hashmap = HashMap::new();
        for (k, v) in script_config.env.as_ref().unwrap_or(&default_hashmap) {
            env_vars.insert(k.clone(), v.clone());
        }
        Ok(env_vars)
    }

    fn env_from_headers(&self, headers: &HeaderMap) -> HashMap<String, String> {
        headers
            .iter()
            .map(|(k, v)| {
                (
                    to_screaming_snake_case(&format!("H_{}", k)),
                    v.to_str()
                        .expect("could not convert HeaderValue to string")
                        .to_string(),
                )
            })
            .collect()
    }

    pub async fn execute(&self, req: Request) -> Result<Response> {
        let script_config = self.script_config()?;

        let env_vars = self.env(req.headers(), &script_config)?;
        let req = req
            .into_body()
            // Convert the body into a Stream<Item = Result<_, _>>:
            .into_data_stream()
            // For each item, map the error to a std::io::Error:
            .map(|item| item.map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e)));
        // convert to AsyncRead:
        let mut req = tokio_util::io::StreamReader::new(req);

        let process = Command::new("sh")
            .args(["-c", &format!("{}", self.path.display())])
            .env_clear()
            .envs(env_vars)
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .spawn()?;
        let (mut stdin, stdout, stderr) = (
            process.stdin.unwrap(),
            process.stdout.unwrap(),
            process.stderr.unwrap(),
        );

        //
        // intercept stderr and log with prefixed lines
        // e.g., if the script name is "yak-shaver", then we will see lines like:
        // [yak-shaver] this is a log line
        // [yak-shaver] this is another log line
        //
        // We want this in a tokio::task::spawn, becuase Tokio will eagerly
        // drive the future forward. If we don't do this, then the future will
        // not be driven forward until the very end of this function, which
        // causes a lock-up in some cases.
        let self_name = self.name.clone();
        let stderr = tokio::io::BufReader::new(stderr);
        let mut stderr_lines = stderr.lines();
        let stderr_formatter_future = tokio::task::spawn(async move {
            while let Some(line) = match stderr_lines.next_line().await {
                Ok(l) => l,
                Err(e) => {
                    error!(
                        "[{}] error reading next line of stderr: {}",
                        self_name,
                        e.to_string()
                    );
                    None
                }
            } {
                // print line to _our_ stderr
                trace!("[{}] {}", &self_name, line);
            }
        });

        //
        // pipe incoming HTTP body |> process.stdin
        // For the same reasons as above, we want this in a tokio::task::spawn:
        let stdin_from_body_future =
            tokio::task::spawn(async move { tokio::io::copy(&mut req, &mut stdin).await });

        //
        // setup piping process.stdout |> axum_response
        let mut axum_response = Response::new(Body::from_stream(ReaderStream::new(stdout)));
        if let Some(headers) = &script_config.headers {
            for (k, v) in headers {
                axum_response.headers_mut().insert(
                    HeaderName::from_lowercase(k.to_lowercase().as_bytes())?,
                    HeaderValue::from_str(v)?,
                );
            }
        }

        //
        // Now that we've set up the above, wait for stdin/stderr to finish and
        // then return the axum_response (it is up to axum to wait for stdout
        // to finish)
        stderr_formatter_future.await?;
        stdin_from_body_future.await??;

        Ok(axum_response)
    }
}
