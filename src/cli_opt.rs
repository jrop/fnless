use clap::Parser;
use enum_as_inner::EnumAsInner;
use std::path::PathBuf;

#[derive(Parser, Debug)]
#[clap(author = "Jonathan Apodaca", version = env!("CARGO_PKG_VERSION"))]
pub struct CliOpt {
    #[clap(subcommand)]
    pub subcommand: CliOptSubcommand,
}

#[derive(Parser, Debug, EnumAsInner)]
pub enum CliOptSubcommand {
    /// Install SystemD service
    Install,

    /// Run Server
    Serve(CliOptServe),
}

#[derive(Parser, Debug)]
pub struct CliOptServe {
    #[clap(long, short)]
    pub config_file: Option<PathBuf>,

    #[clap(long, short)]
    pub log_file: Option<PathBuf>,

    #[clap(long, short)]
    pub port: Option<u16>,

    /// The path to the CGI scripts directory (default: /var/fnless/)
    #[clap(long)]
    pub cgi_path: Option<PathBuf>,
}
