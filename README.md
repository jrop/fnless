![fnless logo](./brand/cover.png)

# fnless

> The simplest CGI/serverless engine you will ever use

## Install

```sh
cargo install --git https://gitlab.com/jrop/fnless --root /usr/local/bin/
sudo fnless install # Installs, enables, and starts the associated SystemD service
```

By default the server listens on port 4545.

## Config

```toml
# /etc/fnless/config.toml
# This file is optional
port = 4545
cgi_path = "/var/fnless/"
```

## Use

Put your CGI-scripts in `/var/fnless`:

```sh
#!/bin/sh
# /var/fnless/hello-world

# messages written to stderr will be logged
>&2 echo "this is a log message"
>&2 echo "this is another log message"

# stdout forms the response body
echo "Hello world!"
```

Now you can curl your endpoint: `curl http://yourhost:4545/hello-world`

Each function is:
1. An executable file in `/var/fnless/<somescript>`
    * The request body is piped to stdin
    * Headers are mapped to environment variables: `H_${HEADER_NAME}` (e.g. `H_CONTENT_TYPE`)
2. A config `/var/fnless/<somescript>.toml` (see [example config](./examples/var-fnless/hello-world.toml))

## Logs

```sh
journalctl -fu fnless
```

## License (MIT)

 The MIT License (MIT)

Copyright © 2021 <jrapodaca@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
